package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.AdresseDto;
import com.exo2.Exercice2.entity.Adresse;
import com.exo2.Exercice2.mapper.AdresseMapper;
import com.exo2.Exercice2.repository.AdresseRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AdresseService {
    private final AdresseRepository adresseRepository;
    private final AdresseMapper adresseMapper;

    public List<AdresseDto> findAll(int page, int size) {
        Page<Adresse> adressesPage = adresseRepository.findAll(PageRequest.of(page, size));
        return adresseMapper.toDtos(adressesPage.getContent());
    }

    @Cacheable("adresses")
    public AdresseDto findById(Long id) {
        return adresseMapper.toDto(adresseRepository.findById(id).orElse(null));
    }

    @Cacheable("adressesParVille")
    public List<AdresseDto> findByVille(String ville) {
        return adresseMapper.toDtos(adresseRepository.findAdresseByVille(ville));
    }
}
