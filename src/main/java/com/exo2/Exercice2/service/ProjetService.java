package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.EtudiantDto;
import com.exo2.Exercice2.dto.ProjetDto;
import com.exo2.Exercice2.entity.Projet;
import com.exo2.Exercice2.mapper.EtudiantMapper;
import com.exo2.Exercice2.mapper.ProjetMapper;
import com.exo2.Exercice2.repository.ProjetRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProjetService {
    private final ProjetRepository projetRepository;
    private final ProjetMapper projetMapper;
    private final EtudiantMapper etudiantMapper;

    public List<ProjetDto> findAll(int page, int size) {
        Page<Projet> projetsPage = projetRepository.findAll(PageRequest.of(page, size));
        return projetMapper.toDtos(projetsPage.getContent());
    }

    @Cacheable("projets")
    public ProjetDto findById(Long id) {
        return projetRepository.findById(id)
                .map(projetMapper::toDto)
                .orElse(null);
    }

    public ProjetDto save(ProjetDto projetDto) {
        return projetMapper.toDto(projetRepository.save(projetMapper.toEntity(projetDto)));
    }

    @Cacheable("etudiantsParProjet")
    public List<EtudiantDto> findEtudiantsByProjetId(Long id) {
        return etudiantMapper.toDtos(projetRepository.findEtudiantsByProjetId(id));
    }
}
