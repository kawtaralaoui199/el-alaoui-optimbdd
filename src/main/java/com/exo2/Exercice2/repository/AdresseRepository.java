package com.exo2.Exercice2.repository;

import com.exo2.Exercice2.entity.Adresse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Repository
public interface AdresseRepository extends JpaRepository<Adresse, Long> {
    Page<Adresse> findAll(Pageable pageable);

    @Query("SELECT a FROM Adresse a WHERE a.ville = :ville")
    List<Adresse> findAdresseByVille(@Param("ville") String ville);
}
