package com.exo2.Exercice2.repository;

import com.exo2.Exercice2.entity.Etudiant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

@Repository
public interface EtudiantRepository extends JpaRepository<Etudiant, Long> {

    @Query("SELECT e FROM Etudiant e WHERE e.nom = :nom AND e.prenom = :prenom")
    Optional<Etudiant> findOneEtudiantByNomAndPrenom(@Param("nom") String nom, @Param("prenom") String prenom);
}
