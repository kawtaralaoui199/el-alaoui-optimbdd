package com.exo2.Exercice2.repository;

import com.exo2.Exercice2.entity.Ecole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Repository
public interface EcoleRepository extends JpaRepository<Ecole, Long> {
    Page<Ecole> findAll(Pageable pageable);

    @Query("SELECT e FROM Ecole e JOIN e.etudiants et WHERE et.nom = :nomEtudiant")
    List<Ecole> findEcolesFromNomEtudiant(@Param("nomEtudiant") String nomEtudiant);
}
