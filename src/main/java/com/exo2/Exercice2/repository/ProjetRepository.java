package com.exo2.Exercice2.repository;

import com.exo2.Exercice2.entity.Etudiant;
import com.exo2.Exercice2.entity.Projet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Repository
public interface ProjetRepository extends JpaRepository<Projet, Long> {
    Page<Projet> findAll(Pageable pageable);

    @Query("SELECT p.etudiants FROM Projet p WHERE p.id = :projetId")
    List<Etudiant> findEtudiantsByProjetId(@Param("projetId") Long projetId);
}
